// Load the iframe API player asynchronously
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Create an iframe and YouTube player after API code downloads

function onYouTubeIframeAPIReady() {		  
  if(onAPIReady !== null){
	  onAPIReady();
  }
}

// Stop videos playing on click events

var stopAll = function() {
    for (var i = 0; i < players.length; i++) {
        if (players[i].getIframe().contentWindow != null && typeof (players[i].pauseVideo) != "undefined") {
            players[i].pauseVideo();
		};
    };
};