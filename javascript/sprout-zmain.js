/**
 * Created by thilina.perera on 3/7/2015.
 */
(function( $ ) {

    $.fn.tabs = function() {
        return this.each(function() {
            //console.log($(this));
            var holder = $(this);
            var navigationHolder = holder.children('.navigation-tabs');
            var panelsHolder = holder.children('.tabs-panels');

            navigationHolder.children('li').each(function (index) {
                $(this).attr('data',index+1)
            });
            panelsHolder.children('li').each(function (index) {
                $(this).attr('data',index+1)
            });
            navigationHolder.children('li').children('a').click(function () {
                var parant_li = $(this).parent();
                var index_li = parant_li.attr('data');
                navigationHolder.children('li').each(function () {
                    $(this).removeClass('active');
                    //$(this).addClass('inactive');
                });
                //parant_li.removeClass('inactive');
                parant_li.addClass('active');

                // panel showing
                panelsHolder.children('li').each(function () {
                    var index_panel = $(this).attr('data');
                    if(index_li == index_panel){
                        panelsHolder.children('li').removeClass('active');
                        //panelsHolder.children('li').addClass('inactive');
                        panelsHolder.children('li').hide();
                        //$(this).removeClass('inactive');
                        $(this).addClass('active');
                        $(this).show();
                    }
                })

            });

            var checker = false;

            navigationHolder.children('li').each(function () {

                if($(this).hasClass('active')){
                    checker = true;
                    // get data id
                    var id = $(this).attr('data');
                    panelsHolder.children('li').each(function () {
                        if($(this).attr('data') == id){
                            //alert(id +'  '+ $(this).attr('data'));
                            panelsHolder.children('li').removeClass('active');
                            //panelsHolder.children('li').addClass('inactive');
                            panelsHolder.children('li').hide();
                            $(this).show();
                            $(this).addClass('active');
                        }
                    })
                }

            });

            if(!checker){
                console.log('thi thing called');
                navigationHolder.children('li').first().addClass('active');
                panelsHolder.children('li').first().addClass('active');
                panelsHolder.children('li').first().show();
            }

        });

    };
}( jQuery ));


$('#module-2-video-trigger').click(function(e) {
    e.preventDefault();
    $('#module-2-video-overlay').fadeIn(500,function(){
        var elem = $(this);
        $(this).children('.close-container').children('.close-button').click(function(){
            elem.fadeOut();
        })
});
    //
    //if($('#module-2-video-overlay .container').height() >= globals.lastWindowHeight) {
    //    var sectionPos = $(".sprout-more").position();
    //    var overlayHeight = $('#signup-overlay .container').height() + 100;
    //    var overlayAbsPos = sectionPos.top;
    //    $("#signup-overlay").css({ "position": "absolute", "height": overlayHeight, "top": overlayAbsPos });
    //} else {
    //    $("#signup-overlay").css({ "position": "fixed", "height": overlayHeight, "top": "0" });
    //}

});




$(function() {

    var isAndroid = navigator.userAgent.toLowerCase().indexOf("android");
    if(isAndroid > -1) {
        if($(window).width() > 700) {
            $('body').addClass('android-track');
        }

    }



    function setupSlider(){
        if($('.module-20').is(':visible')) {
            //if(sliderActive === true) {
                module19SliderOne.destroySlider();
                module19SliderTwo.destroySlider();
                module19SliderThree.destroySlider();
            //}
        }

    }

    $('.tabs').tabs();
    enquire.register("screen and (min-width:46em)", {

        match : function() {  //alert('de');
            $('.tabsSp').tabs();

            if($(window).width() > 1280) {
                skrollTabs();
            }

            console.log('desktop');
            $('body').removeClass('accordian-active');
            if($('.module-20').is(':visible')) {
                //load slides
                module19SliderOne = $('#module19Slider').bxSlider(
                    {
                        onSliderLoad: function(currentIndex){
                            $('#module19Slider').parent().parent().find('.bx-pager .bx-pager-item').each(function() {
                                var setId = $(this).find('.bx-pager-link').attr('data-slide-index');
                                $(this).find('.bx-pager-link').attr('id', setId);
                                var sliderIcon =  $('#module19Slider li#' +setId).attr('data-icon');
                                $(this).find('.bx-pager-link').css('background-image', 'url(' + sliderIcon + ')');
                            });
                            sliderActive = true;

                        },
                        responsive: false
                    }
                );
                module19SliderTwo= $('#module19Slider2').bxSlider(
                    {
                        onSliderLoad: function(currentIndex){
                            $('#module19Slider2').parent().parent().find('.bx-pager .bx-pager-item').each(function() {
                                var setId = $(this).find('.bx-pager-link').attr('data-slide-index');
                                $(this).find('.bx-pager-link').attr('id', setId);
                                var sliderIcon =  $('#module19Slider2 li#' +setId).attr('data-icon');
                                $(this).find('.bx-pager-link').css('background-image', 'url(' + sliderIcon + ')');
                            });
                            sliderActive = true;

                        },
                        responsive: false
                    }
                );
                module19SliderThree= $('#module19Slider3').bxSlider(
                    {
                        onSliderLoad: function(currentIndex){
                            $('#module19Slider3').parent().parent().find('.bx-pager .bx-pager-item').each(function() {
                                var setId = $(this).find('.bx-pager-link').attr('data-slide-index');
                                $(this).find('.bx-pager-link').attr('id', setId);
                                var sliderIcon =  $('#module19Slider3 li#' +setId).attr('data-icon');
                                $(this).find('.bx-pager-link').css('background-image', 'url(' + sliderIcon + ')');
                            });
                            sliderActive = true;

                        },
                        responsive: false
                    }
                );
            }
        }

    });

    enquire.register("screen and (max-width:29.5em)", {
        match :function(){
            if($('.module-20').is(':visible')) {
                $('body').addClass('accordian-active');
            }
            appsAccordian();
            moduleImageAction();
        }
    });

    enquire.register("screen and (max-width:65em)", {
        match :function(){
            //alert('2323');
            skrollr.init({ forceHeight: false });
            skrollr.init().destroy();
            $('body').css('overflow', 'visible');
        }
    });

    $('.module-20-tab-padding li').click(function(){
       module19SliderOne.reloadSlider();
       module19SliderTwo.reloadSlider();
       module19SliderThree.reloadSlider();
    });

   /* if($('.accordian-active').length > 0) {

        $('.tabs-panels .slide .creativitySlider').slideUp();
        $('.tabs-panels .slide:first .creativitySlider').slideDown(1000);
        $('.tabs-panels .slide:first').addClass('active');
    }*/

    menu();
    $('#apps-store').bxSlider();

    function appsAccordian () {

        if($('.accordian-active').length > 0) {
            $('.tabs-panels .slide .creativitySlider').hide();
            $('.tabs-panels .slide:first .creativitySlider').show();
            $('.tabs-panels .slide:first').addClass('active');
            $('.tabs-panels .slide .mobile-title').click(function () {
                //$('.tabs-panels .slide .creativitySlider').not($(this).parent().find('.creativitySlider')).hide();
                //$('.tabs-panels .slide .creativitySlider').hide();
                //$('.tabs-panels .slide').removeClass('active');
                if($(this).parent().find('.creativitySlider').is(':visible')){
                    $(this).parent().find('.creativitySlider').slideUp(500);
                    $(this).parent().removeClass('active');
                } else {
                    $('.tabs-panels .slide .creativitySlider').hide();
                    $('.tabs-panels .slide').removeClass('active');
                    $(this).parent().find('.creativitySlider').slideDown(500);
                    $(this).parent().addClass('active');
                }

                $('html, body').animate({
                    scrollTop: $('.module-20').offset().top-500
                }, 300);
            })
        }
    }

    if($('.locale-stripe').length > 0) {
        if($('.parallax-after').length > 0 ){
            $('.locale-stripe').detach().appendTo('.parallax-after');
        }

    }
    if($('.footer').length > 0) {
        if($('.parallax-after').length > 0 ){
            $('.footer').detach().appendTo('.parallax-after');
        }

    }
    if($(window).width() > 768) {

    }

    if($('.nano').length > 0){
        $(".nano").nanoScroller();
        $(".nano").nanoScroller({ alwaysVisible: true });
    }

});


$(window).on('resize', function () {
    //responsiveSkrollr();
});


$(window).scroll(function () {
    //Add fixed header on scroll
    if ($(this).scrollTop() > 72) {
        var stripeheight = $('header section.sproutNavigationContent').height();
        $('header section.sproutNavigationContent').addClass('fixed');
        if (!/iPhone/i.test(navigator.userAgent)) {
            $('.module-1 .container h2').removeClass('mobile-banner-text');
        }
        //$('.full-page-content').css('margin-top', stripeheight);
    } else {
        if (!/iPhone/i.test(navigator.userAgent)) {
            $('header section.sproutNavigationContent').removeClass('fixed');
        }
        $('.module-1 .container h2').addClass('mobile-banner-text');
        //$('.full-page-content').css('margin-top', 0);
    }
});



function skrollTabs() {
    if($('.parallax_wrap').is(':visible') && $('.parallax-after').is(':visible')) {
    var h = $('.parallax-after').outerHeight();
    var t = $('.parallax-after').offset().top;
    var h1 = $('.parallax_wrap').outerHeight();
    //$('.parallax_wrap').css('height', '830px');

        var s = skrollr.init({
            render: function (data) {
                var index = 0, shift_value = 0;
                //console.log(data.curTop);

                if (data.curTop < 1800) {
                    index = 0;
                } else if (data.curTop < 2200) {
                    index = 1;
                } else if (data.curTop < 2800) {
                    index = 2;
                }

                if ((data.curTop >= 1121) && (data.curTop <= 2600)) {

                    $('.parallax-after, .parallax_wrap').removeClass('back-state');
                    $('.parallax-after, .parallax_wrap').addClass('active');
                    $('.parallax-after').css('top', h1);

                    listItems = $('.module-ksp-features .navigation-tabs li');
                    featureItems = $('.tabs-panels li').not('.tabs-panels li .content li');
                    for (var i = 0; i < listItems.length; i++) {
                        //console.log(data.curTop);
                        if (i !== index) {
                            $(listItems[i]).removeClass('active');
                            $(featureItems[i]).hide();
                            $(featureItems[i]).removeClass('active');
                            //$(featureItems[i]).fadeOut(400, function () {
                            //    $(this).removeClass('active');
                            //});

                            var feature = /(\s)*(feature-.*?)(?=\s)/g;
                            //$('.container.tabs')[0].className = $('.container.tabs')[0].className.replace(feature, '');
                        }
                    }


                    //var featurebg = 'feature-'+index;
                    $(listItems[index]).addClass('active');
                    $(featureItems[index]).show();
                    $(featureItems[index]).addClass('active');

                    //$(featureItems[index]).fadeIn(400, function () {
                    //    $(this).addClass('active');
                    //});
                    //$('.container.tabs').addClass(featurebg);
                }





                if (data.curTop >= 2550) {
                    var f = $('.parallax-after').offset().top;
                    $('.parallax_wrap').removeClass('active').addClass('p-back-state');
                    $('.parallax-after').removeClass('active').addClass('back-state');
                    $('.parallax-after').css('top', h1 + 615);
                    $('.parallax_wrap').css('top', '1388px');
                } else if (data.curTop <= 1121) {
                    $('.parallax-after, .parallax_wrap').removeClass('active');
                    $('.parallax_wrap').removeClass('p-back-state');
                    $('.parallax-after, .parallax_wrap').removeAttr('style');
                }

                $('.module-ksp-features #videoTabs li #tab1').click(function() {
                    window.scrollTo(0, 1500);
                });
                $('.module-ksp-features #videoTabs li #tab2').click(function() {
                    window.scrollTo(0, 1850);
                });
                $('.module-ksp-features #videoTabs li #tab3').click(function() {
                    window.scrollTo(0, 2230);
                });


            }
        });

    }

}

function responsiveSkrollr() {
    if($('.parallax_wrap').is(':visible') && $('.parallax-after').is(':visible')) {
        if ($(window).width() <= 650) {
            skrollr.init({ forceHeight: false });
            skrollr.init().destroy();
            $('body').css('overflow', 'visible');
            $('.parallax_wrap, .parallax-after').removeAttr('style');
            $('.parallax_wrap').removeClass('p-back-state, active');
            $('.parallax-after').removeClass('back-state, active')
        } else {
            skrollTabs();
        }
    }
}

var menu = function () {
    //console.log('k')
    // get navigation

    var elem = $('.navigationToggle .mobile');
    var sproutNavigation = elem.parent().parent();
    var mobileNavigation = sproutNavigation.children('.navigation');
    var click_elem = elem.children('.mobileTrigger');
    var click_parent = click_elem.parent();


    click_elem.click(function(e){
        e.preventDefault();
        e.stopPropagation();
        if(click_parent.hasClass('toggled')){
            click_parent.removeClass('toggled');
            mobileNavigation.slideUp('fast');
            click_elem.children('.arrow').removeClass('down');
            click_elem.children('.arrow').addClass('up')
        }else{
            click_parent.addClass('toggled');
            mobileNavigation.slideDown('fast');
            click_elem.children('.arrow').removeClass('up');
            click_elem.children('.arrow').addClass('down')
        }
    });
    if($('.mobileTrigger').is(':visible')) {

        $(window).bind({
            click: function() {
                mobileNavigation.slideUp('fast');
                click_parent.removeClass('toggled');
                click_elem.children('.arrow').removeClass('down').addClass('up');
            },
            scroll: function() {
                mobileNavigation.slideUp('fast');
                click_parent.removeClass('toggled');
                click_elem.children('.arrow').removeClass('down').addClass('up');
            }
        });

    }


    enquire.register("screen and (max-width:46em)", {

        match : function() {

            mobileNavigation.hide();
            $('#pretzel-video-mobile').show();
            click_parent.removeClass('toggled');
            click_elem.children('.arrow').removeClass('down');
            click_elem.children('.arrow').addClass('up')

        },      // OPTIONAL
        // If supplied, triggered when the media query transitions
        // *from an unmatched to a matched state*

        unmatch : function() {

            mobileNavigation.show();
            $('#pretzel-video-mobile').hide();

        },    // OPTIONAL
        // If supplied, triggered when the media query transitions
        // *from a matched state to an unmatched state*.
        // Also may be called when handler is unregistered (if destroy is not available)

        setup : function() {},      // OPTIONAL
                                    // If supplied, triggered once immediately upon registration of the handler

        destroy : function() {},    // OPTIONAL
                                    // If supplied, triggered when handler is unregistered. Place cleanup code here

        deferSetup : true           // OPTIONAL, defaults to false
                                    // If set to true, defers execution the setup function
                                    // until the media query is first matched. still triggered just once
    });


};


// validation
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

// validation
//$(document).on("submit", function(e) {
//    //console.log('safasf');
//    var sEmail = $('.email_field').val();
//    $(this)
//        .find('input')
//        .filter("input.required")
//        .filter(function() { return this.value == ''; })
//        .each(function() {
//            e.preventDefault();
//            $(this).parent()
//                .addClass('field-required')
//            if($(this).parent().hasClass('formControl')){
//                $(this).parent()
//                    .css({
//                        "border" : '2px solid red'
//                    })
//            }else{
//                $(this).css({
//                    "border" : '2px solid red'
//                })
//            }
//
//            $(this).focus();
//        });
//
//    if (!validateEmail(sEmail)) {
//        e.preventDefault();
//    }
//
//});


$(document).ready(function(){
    $('.formControl').parent().parent().parent().find('.button').click(function(e){
        e.preventDefault();
        $(this).parent().parent().parent().find('input[type=text]').attr('data-placeholder-value','');
        var inputText = $(this).parent().parent().parent().find('input[type=text]').val();
        //alert(inputText);
        if(inputText == "") {
            $(this).parent().parent().parent().find('.formControl').css({
                "border" : '2px solid red'
            });
        }else{
            $( ".search" ).submit();
        }
    });
    if (/iPad|iPod/i.test(navigator.userAgent)) {
        $('.module-2 .container .videoHolder .item .fitHolder .contentContainer').each(function(){
            $(this).on('click touchstart', function () {
                //do nothing
            });
        });

        $('.module-5 .container .tabs-panels .slide .thumbs li .col-full-height,.module-5 .container .tabs-panels .slide .thumbs li .row .col-full, .module-5 .container .tabs-panels .slide .thumbs li .row .col').each(function(){
            $(this).on('click touchstart', function () {
                //do nothing
            });
        });

        $('.module-21 .container .learnUse .one .col-full-height .hoverBoxContainerOne,.module-21 .container .learnUse .rowTwo .row .two .hoverBoxContainerTwo,.module-21 .container .learnUse .rowTwo .row .three .hoverBoxContainerThree,  .module-21 .container .learnUse .rowTwo .row .four .hoverBoxContainerFour').each(function(){
            $(this).on('click touchstart', function () {
                //do nothing
            });
        });
    }
});


var browser = {
    isIe: function () {
        return navigator.appVersion.indexOf("MSIE") != -1;
    },
    navigator: navigator.appVersion,
    getVersion: function() {
        var version = 999; // we assume a sane browser
        if (navigator.appVersion.indexOf("MSIE") != -1)
        // bah, IE again, lets downgrade version number
            version = parseFloat(navigator.appVersion.split("MSIE")[1]);
        return version;
    }
};

if (browser.isIe() && browser.getVersion() == 9) {
    //alert("You are currently using Internet Explorer" + browser.getVersion() + " or are viewing the site in Compatibility View, please upgrade for a better user experience.")
    $('html').addClass('ie9');
}

function moduleImageAction() {
    $('.module-ksp-features .tabs-panels li').each(function() {
        var dataUrl = $(this).find('.product').attr('data-url');
        $(this).find('.product img')
            .wrap('<a href="' + dataUrl + '">');
    });
}


//$(function() {
//    var input = document.createElement("input");
//    if(('placeholder' in input)==false) {
//        $('[placeholder]').focus(function() {
//            var i = $(this);
//            if(i.val() == i.attr('placeholder')) {
//                i.val('').removeClass('placeholder');
//                if(i.hasClass('password')) {
//                    i.removeClass('password');
//                    this.type='password';
//                }
//            }
//        }).blur(function() {
//            var i = $(this);
//            if(i.val() == '' || i.val() == i.attr('placeholder')) {
//                if(this.type=='password') {
//                    i.addClass('password');
//                    this.type='text';
//                }
//                i.addClass('placeholder').val(i.attr('placeholder'));
//            }
//        }).blur().parents('form').submit(function() {
//            $(this).find('[placeholder]').each(function() {
//                var i = $(this);
//                if(i.val() == i.attr('placeholder'))
//                    i.val('');
//            })
//        });
//    }
//});